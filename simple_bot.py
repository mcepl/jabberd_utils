#!/usr/bin/python

import getpass
import logging
import sys

import sleekxmpp

from optparse import OptionParser


if sys.version_info < (3, 0):
    reload(sys)
    sys.setdefaultencoding('utf8')


class EchoBot(sleekxmpp.ClientXMPP):

    def __init__(self, jid, password):
        super(EchoBot, self).__init__(jid, password)

        self.add_event_handler('session_start', self.start)
        self.add_event_handler('message', self.message)

    def start(self, event):
        self.send_presence()
        self.get_roster()

    def message(self, msg):
        if msg['type'] in ('normal', 'chat'):
            # msg.reply('Thanks for sending:\n%s' % msg['body']).send()
            self.send_message(mto=msg['from'],
                              mbody='Thanks for sending:\n%s' % msg['body'])


if __name__ == '__main__':
    optp = OptionParser()

    optp.add_option('-d', '--debug', help='set logging to DEBUG',
                    action='store_const', dest='loglevel',
                    const=logging.DEBUG, default=logging.INFO)
    optp.add_option("-j", "--jid", dest="jid",
                    help="JID to use")
    optp.add_option("-p", "--password", dest="password",
                    help="password to use")

    opts, args = optp.parse_args()

    if opts.jid is None:
        opts.jid = raw_input("Username: ")
    if opts.password is None:
        opts.password = getpass.getpass("Password: ")

    logging.basicConfig(format='%(levelname)-8s %(message)s',
                        level=opts.loglevel)

    xmpp = EchoBot(opts.jid, opts.password)
    xmpp.register_plugin('xep_0030')  # Service Discovery
    xmpp.register_plugin('xep_0199')  # Ping

    if xmpp.connect():
        xmpp.process(block=True)
    else:
        print('Unable to connect')
