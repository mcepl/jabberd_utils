#!/usr/bin/env python3

import argparse
import time
from xml.etree import ElementTree as ET

block_command_el = '''<iq type='set'>
  <block xmlns='urn:xmpp:blocking'>
{}  </block>
</iq>
'''

req_blocklist_el = '''<iq type='get'>
<blocklist xmlns='urn:xmpp:blocking'/>
</iq>
'''

unblock_command_el = '''<iq type='set'>
  <unblock xmlns='urn:xmpp:blocking'>
    <item jid='{}'/>
  </unblock>
</iq>
'''

parser = argparse.ArgumentParser(description='I want to block him!')
parser.add_argument('JID', nargs='+', help='JID to be blocked')

args = parser.parse_args()

block_elements = ''

for block_jid in args.JID:
    block_elements += \
        "          <item jid='{}'/>\n".format(block_jid)

print(block_command_el.format(block_elements))

time.sleep(3)
print(req_blocklist_el)
